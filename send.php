<?php

$name	 = $_POST[ 'name' ];
$email	 = $_POST[ 'email' ];
$message = $_POST[ 'message' ];
$from	 = "Content-type: text/html; charset=UTF-8";
$to		 = "marcinsoja85@gmail.com";
$subject = 'Wiadomosc od ' . $email;
/*$headers = 'From: webmaster(malpa)example.com' . "\r\n" .
    'Reply-To: webmaster(malpa)example.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();*/

$dataReturned	 = array();
$body			 = '<html><body>';
$body .= $name . ' (' . $email . ') <p> wysłał do Ciebie wiadomość o treści:</p> <p>' . $message . '</p>';
$body.= '<a href="mailto:'.$email.'">Odpowiedz nadawcy</a>';
$body.= '</body></html>';

if ( !$name ) {
    $dataReturned[ "name" ] = 'Proszę podać swoje imię';
}

if ( !$email || !filter_var( $_POST[ 'email' ], FILTER_VALIDATE_EMAIL ) ) {
    $dataReturned[ "email" ] = 'Proszę podać poprawy adres email';
}

if ( !$message ) {
    $dataReturned[ "message" ] = 'Proszę wpisać treść wiadomości';
}

if ( empty( $dataReturned ) ) {
    if ( mail( $to, $subject, $body, $from ) ) {
        $dataReturned[ "send" ] = 'Wiadomość została wysłana!';
    } else {
        $dataReturned[ "serverError" ] = 'Wiadomość nie może zostać wysłana.';
    }
}
echo json_encode( $dataReturned, JSON_PRETTY_PRINT );
die();

?>