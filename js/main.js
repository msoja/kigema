var $item = $('.carousel .item');
var $wHeight = $(window).height();
$item.eq(0).addClass('active');
$item.height($wHeight);
$item.addClass('full-screen');

$('.carousel img').each(function() {
    var $src = $(this).attr('src');
    var $color = $(this).attr('data-color');
    $(this).parent().css({
        'background-image' : 'url(' + $src + ')',
        'background-color' : $color
    });
    $(this).remove();
});

$(window).on('resize', function (){
    $wHeight = $(window).height();
    $item.height($wHeight);
});

jQuery('.carousel').carousel({
    interval: 7000,
    pause: "false"
});

$('.page-scroll ').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
            $('html, body').animate({
                scrollTop: target.offset().top -50
            }, 1000);
            return false;
        }
    }
});



$('.more').readmore({
    moreLink: '<a href="#" class="link-more">Rozwin</a>',
    lessLink: '<a href="#" class="link-more">Schowaj</a>',
    collapsedHeight: 130
});

$(document).ready(function() {
    $(".circle-ico").hover(function () {

            $(this).addClass("action");
        }, function () {
            $(this).removeClass("action");
        }
    );
    $('top-link').toggle();


});

$(document).ready(function(){
    ///hover container lang menu
    $("#lang-menu").click(
        function(){
            $(this).addClass("cls-border-lang");
            $(this).children().eq(0).addClass("cls-borderbottom-lang");
            $("#lang-menu ul").stop().slideToggle(100);
        },
        function(){
            $(this).removeClass("cls-border-lang");
            $(this).children().eq(0).removeClass("cls-borderbottom-lang");
            $("#lang-menu ul").stop().slideToggle(100);
        }
    );
    /// click languages
    /*$("#lang-menu ul").on("click", function(){
        //select lang and apply changes
        $lang = $(this).text();
        $("#lang-menu ul li").text($lang);
    });*/

});


$(document).ready(function(){
$( '#submit' ).click( function ( ) {
    event.preventDefault();
    $.ajax( {
        url: "send.php",
        data: {
            'name': $( '#name' ).val(),
            'email': $( '#email' ).val(),
            'message': $( '#message' ).val()
        },
        type: 'POST',
        success: function ( data ) {
            /*         $('.send-message').html('<p>Wiadomo�� zosta�a wys�ana</p>');
             $( '#name' ).val( '' );
             $( '#email' ).val( '' );
             $( '#message' ).val( '' );*/
            var dataReturned = JSON.parse( data );
            if ( dataReturned["send"] ) {
                $( '.send-message' ).before( '<div id="added-before-to-form" class="alert alert-success text-center">' + dataReturned["send"] + '</div>' );
                $( '#name' ).val( '' );
                $( '#email' ).val( '' );
                $( '#message' ).val( '' );

            } else if ( dataReturned["serverError"] ) {
                $( '.send-message' ).before( '<div id="added-before-to-form" class="alert alert-danger text-center">' + dataReturned["serverError"] + '</div>' );
            } else {
                if ( dataReturned["name"] )
                    $( '#name' ).after( '<p class="text-danger text-center">' + dataReturned["name"] + '</p>' );
                if ( dataReturned["email"] )
                    $( '#email' ).after( '<p class="text-danger text-center">' + dataReturned["email"] + '</p>' );
                if ( dataReturned["message"] )
                    $( '#message' ).after( '<p class="text-danger text-center">' + dataReturned["message"] + '</p>' );
            }
        }
        ,
        error: function ( errorThrown ) {
            $( '.send-message' ).before( '<div id="added-before-to-form" class="alert alert-danger text-center">Serwer nie odpowiada. Prosz� spr�wa� za chwil�.</div>' );
        }
    } );
    });

});
/*
    var showChar = 100;
    var ellipsestext = "...";
    var moretext = "Rozwin tekst >";
    var lesstext = "Schowaj";


    $('.more').each(function() {
        var content = $(this).html();

        if(content.length > showChar) {

            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);

            var html = c + '<span class="moreellipses">' + ellipsestext+ '</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';

            $(this).html(html);
        }

    });

    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });

});
*/


